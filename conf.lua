function love.conf(t)
	t.title = "Wizard vs Warlock"
	t.version = "0.9.2"
	t.window.width = 960
	t.window.height = 640
end

package.lovepaths = {"lib/%.lua", "lib/%/init.lua"}

table.insert(package.loaders, function(modulename)
	modulename = modulename:gsub("%.", "/")

	for _, v in ipairs(package.lovepaths) do
		local filename = v:gsub("%%", modulename)
		if love.filesystem.isFile(filename) then
			return love.filesystem.load(filename)
		end
	end
end)
