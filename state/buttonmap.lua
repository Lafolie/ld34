local map = {}

map.mapQueue = {}
map.inState = false

function map:enter(previous)
	self.buttonsImg = cache.image "gfx/ui/buttons.png"
	self.font = cache.font "gfx/square.ttf:32"
	self.bg = cache.image "gfx/combobg.png"

	self.buttonAQuad = love.graphics.newQuad(0, 0, 16, 16, 32, 32)
	self.buttonBQuad = love.graphics.newQuad(16, 0, 16, 16, 32, 32)

	self.inState = true
	self:next()
end

function map:leave()
	self.buttonImg = nil
	self.font = nil
	self.inState = false
end

function map:queue(manager, name)
	table.insert(self.mapQueue, {manager, name})
	if not self.inState then
		Gamestate.push(self)
	end
end

function map:next()
	if #self.mapQueue == 0 then return Gamestate.poplate() end

	local manager, name = unpack(table.remove(self.mapQueue, 1))
	self.first = true
	self.colorFade = 0
	self.mapping = {}
	self.manager = manager
	self.name = name
	self.keyDown = nil
	self.countdown = nil
end

function map:update(dt)
	self.colorFade = self.colorFade + dt
	if self.countdown then
		self.countdown = self.countdown - dt
		if self.countdown <= 0 then
			self:next()
		end
	end
end

local function lerp(from, to, pos)
	return (to-from)*pos+from
end

local function colerp(from, to, pos)
	return lerp(from[1], to[1], pos), lerp(from[2], to[2], pos), lerp(from[3], to[3], pos)
end

function map:setHighlightColor(highlit)
	if highlit then
		local pos = math.min(math.abs(1/(self.colorFade%2-1))/10, 1)
		love.graphics.setColor(colerp(white, highlightcolor, pos))
	else
		love.graphics.setColor(white)
	end
end

function map:keypressed(key)
	self.keyDown = key
end

function map:keyreleased(key)
	-- If this is leftover from another state, or we've pressed multiple buttons, skip
	if key ~= self.keyDown then return end

	table.insert(self.mapping, key)
	if #self.mapping == 2 then
		self:done()
	end
end

function map:joystickreleased(joystick, button)
	local id, instance = joystick:getID()
	local mapping = ("j%d-%d-%d"):format(id, instance, button)
	table.insert(self.mapping, mapping)
	if #self.mapping == 2 then
		self.countdown = 1
	end
end

function map:done()
	self.manager:setButton("a", self.mapping[1])
	self.manager:setButton("b", self.mapping[2])
	self.manager:save()

	self.countdown = 1
end

map.buttonpos =
{
	vector(60, 40),
	vector(164, 40),
}

map.textOff = vector(16, 24)

function map:textWithBackground(text, x, y, length)
	local textLength = self.font:getWidth(text)/4
	local center = x + length/2
	x = center - textLength/2

	love.graphics.setColor(0, 0, 0, 100)
	love.graphics.rectangle("fill", x-2, y-2, textLength+4, self.font:getHeight()/4+4)

	love.graphics.setColor(white)
	love.graphics.print(text, x, y)
end

function map:draw()
	love.graphics.draw(self.bg)
	love.graphics.setFont(self.font)
	self:textWithBackground("Please map the controls for " .. self.name, 5, 10, 230)

	if self.countdown then
		self:textWithBackground("Saving...", 5, 140, 230)
	end

	love.graphics.draw(self.buttonsImg, self.buttonAQuad, self.buttonpos[1]:unpack())
	love.graphics.draw(self.buttonsImg, self.buttonBQuad, self.buttonpos[2]:unpack())

	for i, v in ipairs(self.mapping) do
		self:textWithBackground(v:upper(), self.buttonpos[i].x-self.textOff.x/2+8, self.buttonpos[i].y+self.textOff.y, self.textOff.x)
	end
end

return map
