local fightAnimation = {}

fightAnimation.turnLength = 3

function fightAnimation:enter(previous, move1, move2)
	self.previous = previous
	self.move1 = move1 and moves[move1](previous.players[1]:getFront(), 1, previous.players[1], previous.players[2])
	self.move2 = move2 and moves[move2](previous.players[2]:getFront(), -1, previous.players[2], previous.players[1])
	self.moves = {[1] = self.move1, [2] = self.move2}

	self.progress = 0
	self.effectsApplied = false
	self.blocksApplied = false

	if not self.move1 and not self.move2 then
		self.progress = 0.9
	end

	for i = 1, 2 do
		if self.moves[i] then
			previous.players[i]:addMessage(self.moves[i]:getName())
			previous.players[i].state = "casting"
		end
	end
end

function fightAnimation:update(dt)
	self.previous:update(dt)
	self.progress = self.progress + dt/self.turnLength

	local players = self.previous.players
	if self.progress >= 0.40 and not self.blocksApplied then
		self.blocksApplied = true
		self.blocked1 = self.move1 and self.move2 and self.move2:block(self.move1)
		self.blocked2 = self.move1 and self.move2 and self.move1:block(self.move2)
		if self.move1 then self.move1:preProc(players[1], players[2]) end
		if self.move2 then self.move2:preProc(players[2], players[1]) end

		if self.blocked1 then
			players[1]:addMessage("Blocked!")
			self.move1:blocked()
		end
		if self.blocked2 then
			players[2]:addMessage("Blocked!")
			self.move2:blocked()
		end
	end

	if self.progress >= 0.95 and not self.effectsApplied then
		self.effectsApplied = true
		self.waitingForHit = false

		if self.move1 and not self.blocked1 then
			self.move1:proc(players[1], players[2])
			self.waitingForHit = self.waitingForHit or self.move1.hasHitAnimation
		end
		if self.move2 and not self.blocked2 then
			self.move2:proc(players[2], players[1])
			self.waitingForHit = self.waitingForHit or self.move2.hasHitAnimation
		end
	end

	if self.progress >= 1.2 then
		self.waitingForHit = false
	end

	if self.progress >= 1.0 and not self.waitingForHit then
		return Gamestate.poplate()
	end
end

function fightAnimation:draw()
	self.previous:draw()

	for i = 1, 2 do
		if self.moves[i] and (self.progress <= 1 or self.moves[i].hasHitAnimation) then
			self.moves[i]:draw(self.progress)
		end
	end
end

return fightAnimation
