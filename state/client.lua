local client = {}

function client:enter(previous, ip, port)
	port = tonumber(port) or 42813

	self.font = cache.font "gfx/square.ttf:32"
	self.bg = cache.image "gfx/combobg.png"

	self.remote = RemoteHost(ip, port)
	self:resume()
end

function client:resume()
	self.remote:waitForGame()
end

function client:update(dt)
	if self.remote:gameStarted() then
		Gamestate.push(states.fight, self.remote)
	end
end

function client:draw()
	local text = "Waiting for host to start game"

	if not self.remote:isConnected() then
		text = "Connecting"
	end

	local xpos = 120-self.font:getWidth(text)/8
	text = text .. ("."):rep(love.timer.getTime()%4)

	love.graphics.draw(self.bg)
	love.graphics.setFont(self.font)
	love.graphics.print(text, xpos, 40)
end

return client
