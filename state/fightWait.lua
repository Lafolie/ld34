local fightWait = {}

fightWait.turnLength = 5

function fightWait:enter(previous)
	self.previous = previous
	self.turnTimer = self.turnLength

	self.sfx =
	{
		inputShort = cache.soundData "sfx/inputShort.ogg",
		inputLong = cache.soundData "sfx/inputLong.ogg",
	}
end

function fightWait:leave()
	self.sfx = nil
end

function fightWait:update(dt)
	self.previous:update(dt)

	self.turnTimer = math.max(self.turnTimer - dt, 0)
	if self.turnTimer <= 0 then
		return Gamestate.poplate()
	end
end

function fightWait:shortpress(tag, button)
	love.audio.newSource(self.sfx.inputShort):play()
	return self.previous.combo:shortpress(tag, button)
end

function fightWait:longpress(tag, button)
	return self.previous.combo:longpress(tag, button)
end

function fightWait:longpressready(tag, button)
	love.audio.newSource(self.sfx.inputLong):play()
end

function fightWait:draw()
	self.previous:draw()

	love.graphics.printf(("%.1f"):format(self.turnTimer), 80, 5, 80, "center")
end

return fightWait
