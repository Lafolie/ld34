local fight = {}

fight.remotePlayer = nil

function fight:enter(prev, opponent)
	self.players =
	{
		Player(self:getPlayerPosition(1), 1, "Wizard"),
		Player(self:getPlayerPosition(2), -1, "Warlock"),
	}

	self.p1 = InputManager("p1", "Player 1")

	self.opponent = opponent or self.remotePlayer
	if opponent then
		self.p2 = nil
	else
		self.p2 = InputManager("p2", "Player 2")
	end

	self.combo = ComboManager{"p1", "p2"}
	for i, v in pairs(moves) do
		v:addToComboManager(self.combo)
	end

	self.turnCounter = 0

	self.bg = cache.image "gfx/backgroundPurpleSeamless.png"
	self.font = cache.font "gfx/square.ttf:36"

	local sounddata = cache.soundData "sfx/loop.ogg"
	self.music = love.audio.newSource(sounddata)
	self.music:setVolume(0.4)
	self.music:setLooping(true)
	self.music:play()
end

function fight:leave()
	self.music:stop()
	self.music = nil
	self.bg = nil
	self.font = nil
end

function fight:resume(from, ...)
	if from == states.fightWait then
		self:endTurn()
	elseif from == states.fightAnimation then
		if self:checkWin() then return end
		self:startTurn()
	elseif from == states.fightTransition then
		Gamestate.pushlate(...)
	end
end

function fight:checkWin()
	local p1died = self.players[1].health <= 0
	local p2died = self.players[2].health <= 0

	if p1died and p2died then
		Gamestate.switch(states.win, "Everybody")
	elseif p1died then
		Gamestate.switch(states.win, self.players[2].name)
	elseif p2died then
		Gamestate.switch(states.win, self.players[1].name)
	else
		return false
	end

	return true
end

function fight:startTurn()
	for i = 1, 2 do
		self.players[i].state = "normal"
	end

	self.turnCounter = self.turnCounter + 1
	self.combo:start()
	if self.opponent then
		self.opponent:startTurn()
	end
	Gamestate.pushlate(states.fightTransition, "Input", states.fightWait)
end

function fight:endTurn()
	self.combo:finish()

	local move1 = self.combo:getComboFor("p1")
	local move2 = self.combo:getComboFor("p2")
	if self.opponent then
		self.opponent:opponentMoves(move1)
		move2 = self.opponent:getMove()
	end

	if self.remotePlayer then
		Gamestate.pushlate(states.fightTransition, "Sync", states.fightTransition, "Battle", states.fightAnimation, move1, move2)
	else
		Gamestate.pushlate(states.fightTransition, "Battle", states.fightAnimation, move1, move2)
	end
end

function fight:update(dt)
	if self.turnCounter == 0 then
		self:startTurn()
	end

	self.p1:update(dt)
	if self.p2 then
		self.p2:update(dt)
	end
end

function fight:draw()
	love.graphics.setFont(self.font)

	local xpos = 12*love.timer.getTime()%240
	love.graphics.draw(self.bg, xpos, 0)
	love.graphics.draw(self.bg, xpos-240, 0)

	for i, v in ipairs(self.players) do
		v:draw()
		v:drawMessages()
		v:drawIndicator(i == 1 and 20 or 150, 12)
	end
end

function fight:getPlayerPosition(n)
	if n == 1 then
		return vector(22, 60)
	else
		return vector(220, 60)
	end
end

return fight
