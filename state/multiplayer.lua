local multiplayer = {}

function multiplayer:enter()
	self.remote = RemotePlayer()
	self.font = cache.font "gfx/square.ttf:32"
	self.bg = cache.image "gfx/combobg.png"
end

function multiplayer:leave()
	self.font = nil
	self.bg = nil
end

function multiplayer:update(dt)
	if self.remote:tryAccept() then
		states.fight.remotePlayer = self.remote
		return Gamestate.switch(states.menu)
	end
end

function multiplayer:draw()
	local text = "Waiting for client"
	local xpos = 120-self.font:getWidth(text)/8
	text = text .. ("."):rep(love.timer.getTime()%4)
	love.graphics.draw(self.bg)
	love.graphics.setFont(self.font)
	love.graphics.print(text, xpos, 40)
end

return multiplayer
