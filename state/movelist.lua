local movelist = {}

local numEntries = 6

function movelist:enter()
	self.list = {}
	self.font = cache.font "gfx/square.ttf:32"
	self.bg = cache.image "gfx/combobg.png"

	self.buttonImg = cache.image "gfx/ui/buttons.png"
	self.comboMapping =
	{
		["short_a"] = love.graphics.newQuad( 0,  0, 16, 16, self.buttonImg:getDimensions()),
		["long_a"]  = love.graphics.newQuad( 0, 16, 16, 16, self.buttonImg:getDimensions()),
		["short_b"] = love.graphics.newQuad(16,  0, 16, 16, self.buttonImg:getDimensions()),
		["long_b"]  = love.graphics.newQuad(16, 16, 16, 16, self.buttonImg:getDimensions()),
	}

	for _, m in pairs(moves) do
		if m.combos then
			for i, v in ipairs(m.combos) do
				table.insert(self.list, {m:getName(), v})
			end
		end
		if m.combo then
			table.insert(self.list, {m:getName(), m.combo})
		end
	end

	table.sort(self.list, function(a, b) return a[1] < b[1] end)

	self.startPos = 1
	self.scrollbarLength = (numEntries+1)/#self.list

	love.keyboard.setKeyRepeat(true)
end

function movelist:leave()
	love.keyboard.setKeyRepeat(false)

	self.font = nil
	self.buttonImg = nil
	self.comboMapping = nil
end

function movelist:keypressed(key)
	if key == "down" then
		self.startPos = math.max(math.min(self.startPos + 1, #self.list-numEntries), 1)
	elseif key == "up" then
		self.startPos = math.max(self.startPos - 1, 1)
	end
end

function movelist:drawButtons(buttons, xleft, y, width)
	xleft = xleft + width-#buttons*18
	y = y + 1
	for _, v in ipairs(buttons) do
		love.graphics.draw(self.buttonImg, self.comboMapping[v], xleft, y)
		xleft = xleft + 18
	end
end

movelist.legendPress = {{"short_a"}, {"short_b"}}
movelist.legendHold = {{"long_a"}, {"long_b"}}
movelist.legendPressPos = vector(30, 4)
movelist.legendHoldPos = vector(140, 4)

function movelist:draw()
	love.graphics.draw(self.bg)
	love.graphics.setFont(self.font)

	local evenRowColor = {64, 64, 64, 200}
	local oddRowColor = {160, 160, 160, 200}

	local barHeight = (numEntries+1)*18
	love.graphics.setColor(106, 106, 106)
	love.graphics.rectangle("fill", 210, 25, 10, barHeight)
	love.graphics.setColor(172, 172, 172)
	love.graphics.rectangle("fill", 210, 25+((self.startPos-1)/#self.list)*barHeight, 10, barHeight*self.scrollbarLength)

	for i = self.startPos, math.min(self.startPos+numEntries, #self.list) do
		local v = self.list[i]
		local rowColor = i % 2 == 0 and evenRowColor or oddRowColor
		local ypos = 25+18*(i-self.startPos)

		love.graphics.setColor(rowColor)
		love.graphics.rectangle("fill", 20, ypos, 90, 18)
		love.graphics.setColor(white)
		self:drawButtons(v[2], 20, ypos, 90)

		love.graphics.setColor(rowColor)
		love.graphics.rectangle("fill", 120, ypos, 90, 18)
		love.graphics.setColor(white)
		love.graphics.printf(v[1], 122, ypos+5, 86, "left")
	end

	local sel = love.timer.getTime()%4 > 2 and 1 or 2
	self:drawButtons(self.legendPress[sel], self.legendPressPos.x, self.legendPressPos.y, 16)
	love.graphics.print("Short press", self.legendPressPos.x + 16, self.legendPressPos.y+6)
	self:drawButtons(self.legendHold[sel], self.legendHoldPos.x, self.legendHoldPos.y, 16)
	love.graphics.print("Long press", self.legendHoldPos.x + 16, self.legendHoldPos.y+5)
end

return movelist
