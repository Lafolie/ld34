local win = {}

function win:enter(previous, wintext)
	self.wintext = wintext .. " wins!"
	self.bg = cache.image "gfx/win.png"
	self.font = cache.font "gfx/square.ttf:72"
	self.timer = 3
end

function win:update(dt)
	self.timer = self.timer - dt
	if self.timer <= 0 then
		return Gamestate.switch(states.menu)
	end
end

function win:keypressed(key)
	if key == " " or key == "space" then
		return Gamestate.switch(states.menu)
	end
end

win.textPos = vector(5, 35)
win.textWidth = 230

function win:draw()
	love.graphics.setFont(self.font)
	love.graphics.draw(self.bg)
	love.graphics.printf(self.wintext, self.textPos.x, self.textPos.y, self.textWidth, "center")
end

return win
