local menu = {}

local function keybind(n)
	return function()
		local manager = InputManager("p" .. n)
		states.buttonmap:queue(manager, "Player " .. n)
	end
end

local speeds =
{
	["normal"] = {1, wait = 5, anim = 3},
	["fast"] = {2, wait = 3, anim = 2},
	["slow"] = {3, wait = 7, anim = 4},
	["superfast"] = {4, wait = 2, anim = 1.5},

	[1] = "normal",
	[2] = "fast",
	[3] = "slow",
	[4] = "superfast",
}

local function speedswitch(target)
	local entry
	for i, v in ipairs(menu.main) do
		if v[1]:match("^Game speed:") then
			entry = v
			break
		end
	end
	assert(entry, "Broken speed switch")

	if not target then
		local current = assert(entry[1]:match("%w+$"), "Broken speed switch")
		current = assert(speeds[current], "Broken speed switch")
		current = assert(current[1], "Broken speed switch")
		target = speeds[current%#speeds + 1]
	end

	target = assert(speeds[target], "Speed not found")
	states.fightWait.turnLength = target.wait
	states.fightAnimation.turnLength = target.anim

	entry[1] = "Game speed: " .. speeds[target[1]]
end

menu.main =
{
	{"Singleplayer", function() Gamestate.switch(states.fight, AI()) end},
	{"Multiplayer", function() Gamestate.switch(states.fight, nil) end},
	{"Player 1 keybindings", keybind(1)},
	{"Player 2 keybindings", keybind(2)},
	{"Move list", function() Gamestate.switch(states.movelist) end},
	{"Game speed: normal", speedswitch},
	{"Quit", love.event.quit},
}

function menu:enter(previous, target)
	self.currentMenu = target or "main"
	self.currentEntry = 1
	self.font = cache.font "gfx/square.ttf:34"
	self.bg = cache.image "gfx/menubg.png"
	self.crediFont = cache.font "gfx/square.ttf:14"

	self.blip = cache.soundData "sfx/menu.ogg"
	self.confirm = cache.soundData "sfx/confirm.ogg"

	speedswitch("normal")
end

function menu:leave()
	self.font = nil
	self.blip = nil
	self.confirm = nil
end

function menu:keypressed(key)
	local n = #self[self.currentMenu]
	if key == "down" then
		self.currentEntry = self.currentEntry % n + 1
	elseif key == "up" then
		self.currentEntry = (self.currentEntry - 2) % n + 1
	elseif key == "return" then
		love.audio.newSource(self.confirm):play()
		self[self.currentMenu][self.currentEntry][2]()
		return
	else
		return
	end

	love.audio.newSource(self.blip):play()
end

function menu:draw()
	love.graphics.draw(self.bg)
	love.graphics.setFont(self.font)
	for i, v in ipairs(self[self.currentMenu]) do
		if i == self.currentEntry then
			love.graphics.setColor(highlightcolor)
		end
		love.graphics.printf(v[1], 5, 56 + 12 * i, 230, "center")
		if i == self.currentEntry then
			love.graphics.setColor(white)
		end
	end

	love.graphics.setFont(self.crediFont)
	love.graphics.print("Made for Ludum Dare 34", 2, 150)
	love.graphics.print("By @Day_Lafolie and @bartbes", 2, 155)
end

return menu
