local fightTransition = {}

function fightTransition:enter(prev, name, ...)
	self.prev = prev
	self.next = {n = select("#", ...), ...}
	self.countdown = 1

	self.font = cache.font "gfx/square.ttf:36"

	self.text = name .. " Phase"
	self.endPos = 120-self.font:getWidth(self.text)/8
end

function fightTransition:leave()
	self.font = nil
end

function fightTransition:update(dt)
	self.prev:update(dt)
	self.countdown = self.countdown - dt

	if self.countdown <= 0 then
		Gamestate.poplate(unpack(self.next, 1, self.next.n))
	end
end

function fightTransition:draw()
	self.prev:draw()

	local xpos = math.floor(self.endPos*(1-self.countdown)*2)
	if self.countdown < 0.5 then xpos = self.endPos end

	love.graphics.setFont(self.font)
	love.graphics.print(self.text, xpos, 40)
end

return fightTransition
