class "InputManager"
{
	threshold = 0.5,

	__init__ = function(self, tag, nicename)
		self.buttons = {}
		self.keys = {}
		self.buttonState = {}
		self.tag = tag
		self.filename = "button-" .. tag .. ".conf"
		if love.filesystem.isFile(self.filename) then
			self:load()
		else
			states.buttonmap:queue(self, nicename or tag)
		end
	end,

	setButton = function(self, button, key)
		if self.buttons[button] then self.keys[self.buttons[button]] = nil end
		if self.keys[key] then self.buttons[self.keys[key]] = nil end
		self.keys[key] = button
		self.buttons[button] = key
		self.buttonState[button] = false
	end,

	update = function(self, dt)
		for button, state in pairs(self.buttonState) do
			if self:isDown(self.buttons[button]) then
				self.buttonState[button] = (self.buttonState[button] or 0) + dt
				if self.buttonState[button] >= self.threshold and self.buttonState[button] - dt < self.threshold then
					Gamestate.longpressready(self.tag, button)
				end
			else
				if self.buttonState[button] then self:doPress(button, self.buttonState[button]) end
				self.buttonState[button] = false
			end
		end
	end,

	doPress = function(self, button, duration)
		if duration >= self.threshold then
			Gamestate.longpress(self.tag, button)
		else
			Gamestate.shortpress(self.tag, button)
		end
	end,

	load = function(self)
		local map = love.filesystem.load(self.filename)()
		for button, key in pairs(map) do
			self:setButton(button, key)
		end
	end,

	save = function(self)
		local out = [[return { %s }]]
		local singlebind = [[ [%q] = %q, ]]
		local binds = {}
		for button, key in pairs(self.buttons) do
			table.insert(binds, singlebind:format(button, key))
		end
		love.filesystem.write(self.filename, out:format(table.concat(binds)))
	end,

	isDown = function(self, key)
		if #key > 1 and key:match("^j") then
			local joyid, joyinstance, joybutton = key:match("^j(%d+)%-(%d+)%-(%d+)$")
			assert(joyid and joyinstance and joybutton, "Invalid key binding")
			joyid, joyinstance, joybutton = tonumber(joyid), tonumber(joyinstance), tonumber(joybutton)
			local joysticks = love.joystick.getJoysticks()
			for i, v in ipairs(joysticks) do
				local id, instance = v:getID()
				if id == joyid and instance == joyinstance then
					return v:isDown(joybutton)
				end
			end
			error("Mapped joystick not found, please replug or remap")
		end
		return love.keyboard.isDown(key)
	end,
}
