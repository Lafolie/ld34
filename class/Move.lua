class "Move"
{
	direct = true,
	smallEffect = 1.25,
	largeEffect = 2,
	extraLargeEffect = 5,
	drawing = true,
	hasHitAnimation = false,

	__init__ = function(self, pos, dir, caster, target)
		assert(self.dmg or self.effect, "Move has no damage or effect")
		if self.img then
			self.img = cache.image(self.img)
		end
		if self.sfx then
			self.sfx = cache.soundData(self.sfx)
		end
		self.pos = pos
		self.initialPos = pos
		self.dir = dir
	end,

	addToComboManager = function(self, manager)
		assert(self.combo or self.combos, "Move has no combo")

		if self.combos then
			for i, v in ipairs(self.combos) do
				manager:addCombo(self.__name__, v)
			end
		else
			return manager:addCombo(self.__name__, self.combo)
		end
	end,

	isDirect = function(self)
		return self.direct and self.dmg
	end,

	getDmg = function(self, player, enemy)
		return self.dmg
	end,

	getMultipliedDmg = function(self, player, enemy)
		return self:getDmg(player, enemy) * player.attackMultiplier * enemy.defenseMultiplier
	end,

	preProc = function(self, player, enemy)
		if self.preEffect then
			self:preEffect(player, enemy)
		end
	end,

	proc = function(self, player, enemy)
		if self.dmg then
			enemy:doDamage(self:getMultipliedDmg(player, enemy))
			player:decay("attackMultiplier")
			enemy:decay("defenseMultiplier")
		end
		if self.effect then
			self:effect(player, enemy)
		end
	end,

	draw = function(self, progress)
	end,

	block = function(self, other)
		return self.direct and other.direct
	end,

	getName = function(self)
		return self.fancyName or self.__name__
	end,

	adjustPos = function(self, progress)
		self.pos = self.initialPos + vector(progress*self.dir*120, 0)
	end,

	blocked = function(self)
		self.drawing = false
	end,
}
