local function matchone(current, combo)
	if #current ~= #combo then return false end
	for i, v in ipairs(current) do
		if combo[i] ~= v then return false end
	end
	return true
end

local function match(current, combos)
	for _, combo in ipairs(combos) do
		if matchone(current, combo[2]) then return combo[1] end
	end
end

class "ComboManager"
{
	__init__ = function(self, tags)
		self.currentCombos = {}
		self.knownCombos = {}
		for i, v in ipairs(tags) do
			self.currentCombos[v] = {}
		end
		self.accepting = false
	end,

	shortpress = function(self, tag, button)
		if not self.accepting then return end
		table.insert(self.currentCombos[tag], "short_" .. button)
	end,

	longpress = function(self, tag, button)
		if not self.accepting then return end
		table.insert(self.currentCombos[tag], "long_" .. button)
	end,

	start = function(self)
		for i, v in pairs(self.currentCombos) do
			self.currentCombos[i] = {}
		end
		self.accepting = true
	end,

	finish = function(self)
		self.accepting = false
	end,

	addCombo = function(self, name, sequence)
		table.insert(self.knownCombos, {name, sequence})
	end,

	getComboFor = function(self, tag)
		return match(self.currentCombos[tag], self.knownCombos)
	end,
}
