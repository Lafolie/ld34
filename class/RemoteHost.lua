local socket = require "socket"

local function msgToTable(msg)
	local t = {}

	for entry in msg:gmatch("[^,%s]+") do
		local key, value = entry:match("^([^:]-):(.+)$")
		if key and value then
			t[key] = value
		end
	end

	return t
end

class "RemoteHost"
{
	__init__ = function(self, ip, port)
		self.socket = socket.tcp()
		self.socket:settimeout(0)
		self.connected = self.socket:connect(ip, port)
		self.gameRunning = false
		self.opponentMove = nil
	end,

	isConnected = function(self)
		if self.connected then return true end

		local recvt, sendt, err = socket.select({}, {self.socket}, 0)
		if #sendt == 0 then return false end

		self.connected = true
		self.socket:settimeout(0.5)
		return true
	end,

	waitForGame = function(self)
		self.gameRunning = false
	end,

	gameStarted = function(self)
		if self.gameRunning then return true end

		local msg = self.socket:receive()
		if not msg then return false end

		self.gameRunning = true
		self:parseMessage(msg)
		return true
	end,

	parseMessage = function(self, msg)
		msg = msgToTable(msg)

		if msg.s == "1" then
			self.gameRunning = true
		end

		if not states.fight.players then return end

		local p1 = states.fight.players[1]
		local p2 = states.fight.players[2]

		p1.health = tonumber(msg.p2h) or p1.health
		p1.attackMultiplier = tonumber(msg.p2a) or p1.attackMultiplier
		p1.defenseMultiplier = tonumber(msg.p2d) or p1.defenseMultiplier
		p2.health = tonumber(msg.p1h) or p2.health
		p2.attackMultiplier = tonumber(msg.p1a) or p2.attackMultiplier
		p2.defenseMultiplier = tonumber(msg.p1d) or p2.defenseMultiplier
		self.opponentMove = msg.om or self.opponentMove
	end,

	receive = function(self)
		local msg = self.socket:receive()
		if msg then self:parseMessage(msg) end
	end,

	startGame = function(self)
	end,

	startTurn = function(self)
		self.opponentMove = nil
		return self:receive()
	end,

	getMove = function(self)
		self:receive()
		return self.opponentMove
	end,

	opponentMoves = function(self, move)
		if move then self.socket:send(move .. "\n") end
	end,
}
