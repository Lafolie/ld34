class "Player"
{
	__init__ = function(self, pos, dir, name)
		self.pos = pos
		self.dir = dir
		self.name = name

		if name == "Warlock" then
			self.img = cache.image "gfx/warlock.png"
		else
			self.img = cache.image "gfx/wizard.png"
		end

		self.imgBar = cache.image "gfx/ui/healthBar.png"
		self.ui = cache.image "gfx/ui/status.png"

		self.imgBar:setWrap("repeat", "clamp")

		self.normalQuad = love.graphics.newQuad(0, 0, 32, 32, 64, 64)
		self.castingQuad = love.graphics.newQuad(32, 0, 32, 32, 64, 64)
		self.damageQuad = love.graphics.newQuad(0, 32, 32, 32, 64, 64)
		self.healthQuad = love.graphics.newQuad(0, 0, 1, 8, 1, 8)

		self.rock = self.dir == 1 and "L" or "R"
		self.rock = cache.image("gfx/rock" .. self.rock .. ".png")

		self.state = "normal"

		self.health = 100
		self.attackMultiplier = 1
		self.defenseMultiplier = 1

		self.messages = {}
	end,

	getFront = function(self)
		return self.pos + vector(self.dir*24, 18)
	end,

	getCenter = function(self)
		return self.pos + vector(self.dir*16, 16)
	end,

	draw = function(self)
		local yoff = math.floor(2.5*math.sin(love.timer.getTime()))
		local quad = self[self.state .. "Quad"]
		local rockx = self.pos.x-48
		if self.dir == -1 then rockx = rockx - 28 end
		love.graphics.draw(self.rock, rockx, self.pos.y+yoff-4)
		love.graphics.draw(self.img, quad, self.pos.x, self.pos.y+yoff, 0, self.dir, 1)
	end,

	healthSize = vector(64, 8),
	atkPos = vector(16, 14),
	defPos = vector(48, 14),
	namePos = vector(0, -10),
	nameWidth = 75,

	drawIndicator = function(self, x, y)
		-- Ui background
		love.graphics.draw(self.ui, x, y)

		-- Health bar fill
		love.graphics.setColor(white)
		self.healthQuad:setViewport(0, 0, self.healthSize.x*self.health/100, 8)
		love.graphics.draw(self.imgBar, self.healthQuad, x+5, y+4)

		love.graphics.print(("%2d"):format(self.attackMultiplier*10), x+self.atkPos.x, y+self.atkPos.y)
		love.graphics.print(("%2d"):format(10/self.defenseMultiplier), x+self.defPos.x, y+self.defPos.y)

		love.graphics.printf(self.name, x+self.namePos.x, y+self.namePos.y, self.nameWidth, "center")
	end,

	drawMessages = function(self)
		for i = #self.messages, 1, -1 do
			local pos = self:drawSingleMessage(unpack(self.messages[i]))
			if not pos then
				table.remove(self.messages, i)
			else
				self.messages[i][2] = pos
			end
		end
	end,

	drawSingleMessage = function(self, message, pos)
		-- More code that makes no sense
		local alpha = 255-(pos / 50)*255
		if type(message) == "string" then
			love.graphics.setColor(black)
		elseif message > 0 then
			message = "-" .. message
			love.graphics.setColor(200, 20, 20, alpha)
		elseif message < 0 then
			message = "+" .. math.abs(message)
			love.graphics.setColor(20, 200, 20, alpha)
		else
			message = tostring(message)
			love.graphics.setColor(black)
		end
		local posx = self.pos.x
		if self.dir == -1 then posx = posx - self.img:getWidth() end
		love.graphics.print(message, posx, self.pos.y - pos)
		pos = pos + love.timer.getDelta()*25
		if pos >= 50 then pos = nil end

		love.graphics.setColor(white)
		return pos
	end,

	decay = function(self, stat)
		local avg = (self[stat] + 1)/2
		if math.abs(avg - 1) < 0.1 then
			avg = 1
		end
		self[stat] = avg
	end,

	doDamage = function(self, dmg)
		dmg = math.max(math.min(dmg, self.health), self.health-100)
		self.health = self.health - dmg
		table.insert(self.messages, {math.floor(dmg+0.5), 0})

		if dmg > 0 then
			self.state = "damage"
		end
	end,

	addMessage = function(self, msg, ...)
		table.insert(self.messages, {msg:format(...), 0})
	end,
}
