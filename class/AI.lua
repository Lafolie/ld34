class "AI"
{
	__init__ = function(self)
		self.movelist = {}
		for name, _ in pairs(moves) do
			table.insert(self.movelist, name)
		end
	end,

	startGame = function(self)
	end,

	startTurn = function(self)
	end,

	getMove = function(self)
		local n = love.math.random(#self.movelist)
		return self.movelist[n]
	end,

	opponentMoves = function(self, move)
	end,
}
