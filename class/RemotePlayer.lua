local socket = require "socket"

local function fmtTable(t)
	local parts = {}
	for i, v in pairs(t) do
		table.insert(parts, ("%s:%s"):format(i, tostring(v)))
	end
	return table.concat(parts, ", ") .. "\n"
end

class "RemotePlayer"
{
	__init__ = function(self)
		self.socket = socket.bind("*", 42813)
		self.socket:settimeout(0)
	end,

	tryAccept = function(self)
		self.client = self.socket:accept()
		if self.client then
			self.client:settimeout(0.5)
			return true
		end
		return false
	end,

	startGame = function(self)
		self.client:send(fmtTable{s=1})
	end,

	startTurn = function(self)
		local p1 = states.fight.players[1]
		local p2 = states.fight.players[2]
		self.client:send(fmtTable
		{
			p1h = p1.health,
			p1a = p1.attackMultiplier,
			p1d = p1.defenseMultiplier,
			p2h = p2.health,
			p2a = p2.attackMultiplier,
			p2d = p2.defenseMultiplier,
		})
	end,

	getMove = function(self)
		return self.client:receive()
	end,

	opponentMoves = function(self, move)
		self.client:send(fmtTable
		{
			om = move or "",
		})
	end,
}
