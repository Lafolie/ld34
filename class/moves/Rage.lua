return class.private "Rage" (Move)
{
	combos =
	{
		{ "short_a", "short_a", "short_b" },
		{ "short_b", "short_b", "short_b" },
	},
	direct = false,

	effect = function(self, player, enemy)
		enemy.attackMultiplier = enemy.attackMultiplier / self.smallEffect
		player.attackMultiplier = player.attackMultiplier * self.smallEffect
	end,

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		local particleImg = cache.image "gfx/spell/particle_fire.png"
		self.particles = love.graphics.newParticleSystem(particleImg, 512)
		self.particles:setParticleLifetime(0.02, 0.3)
		self.particles:setEmissionRate(500)
		self.particles:setEmitterLifetime(0.1)
		self.particles:setRadialAcceleration(0, 30)
		self.particles:setSizes(0.2, 0.6)
		self.particles:setSpeed(30, 50)
		self.particles:setSpin(0, 10)
		self.particles:setTangentialAcceleration(0, 20)
		self.particles:setSpread(2*math.pi)
		self.particles:setPosition(target:getCenter():unpack())

		self.prev = 0

		local sfx = cache.soundData "sfx/fire.ogg"
		love.audio.newSource(sfx):play()
	end,

	draw = function(self, progress)
		if not self.drawing then return end

		local dt = progress - self.prev
		self.prev = progress
		self.particles:update(dt)

		love.graphics.draw(self.particles)
	end,
}
