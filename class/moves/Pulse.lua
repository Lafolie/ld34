local anim8 = require "anim8"

return class.private "Pulse" (Move)
{
	combos =
	{
		{ "long_a", "long_b" },
		{ "long_b", "long_a" },
	},
	dmg = 15,
	img = "gfx/spell/pulse.png",

	preEffect = function(self, player, enemy)
		player.attackMultiplier = player.attackMultiplier / self.smallEffect
	end,

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		local grid = anim8.newGrid(16, 16, 64, 32, 0, 0, 0)
		self.anim = anim8.newAnimation(grid("1-4", 1, "1-4", 2), 0.05, self.loopf)
		self.prev = 0

		local sfx = cache.soundData "sfx/fireShort.ogg"
		love.audio.newSource(sfx):play()
	end,

	draw = function(self, progress)
		if not self.drawing then return end

		local dt = progress-self.prev
		self.prev = progress
		self.anim:update(dt)
		self:adjustPos(progress)
		self.anim:draw(self.img, self.pos.x, self.pos.y-8, 0, self.dir, 1)
	end,

	loopf = function(anim)
		anim:gotoFrame(4)
	end,
}
