local anim8 = require "anim8"

return class.private "Fireslash" (Move)
{
	combo = { "long_b", "long_b", "long_b" },
	dmg = 30,
	img = "gfx/spell/fireslash.png",

	effect = function(self, player, enemy)
		player.defenseMultiplier = player.defenseMultiplier * self.extraLargeEffect
	end,

	preEffect = function()
		local sfx = cache.soundData "sfx/whiff.ogg"
		love.audio.newSource(sfx):play()
	end,

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		local grid = anim8.newGrid(64, 64, 256, 192, 0, 0, 0)
		self.anim = anim8.newAnimation(grid('1-4', 1, '1-4', 2, '1-4', 3), 0.05, "pauseAtEnd")
		self.prev = 0.5

		self.pos = target:getCenter() - vector(32, 32)
		if self.dir == -1 then
			self.pos.x = self.pos.x + 64
		end
	end,

	draw = function(self, progress)
		if not self.drawing then return end
		if progress < 0.5 then return end

		local dt = progress - self.prev
		self.prev = progress
		self.anim:update(dt)

		self.anim:draw(self.img, self.pos.x, self.pos.y, 0, self.dir, 1)
	end,
}
