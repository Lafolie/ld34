local anim8 = require "anim8"

return class.private "Barrier" (Move)
{
	combo = {"short_a", "short_a"},
	direct = false,
	img = "gfx/spell/barrier.png",

	block = function(self, other)
		return true
	end,

	effect = function(self, player, enemy)
	end,

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		local grid = anim8.newGrid(64, 64, 256, 128, 0, 0, 0)
		self.anim = anim8.newAnimation(grid('1-4', 1, '1-4', 2), 0.125)
		self.prev = 0

		self.pos = caster.pos - vector(16*self.dir, 16)

		local sfx = cache.soundData "sfx/rain.ogg"
		love.audio.newSource(sfx):play()
	end,

	draw = function(self, progress)
		local dt = progress - self.prev
		self.prev = progress
		self.anim:update(dt)

		self.anim:draw(self.img, self.pos.x, self.pos.y, 0, self.dir, 1)
	end,
}
