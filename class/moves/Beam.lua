return class.private "Beam" (Move)
{
	combo = { "long_a", "long_a", "long_a" },
	dmg = 30,
	img = "gfx/spell/beam.png",

	effect = function(self, player, enemy)
		player.attackMultiplier = player.attackMultiplier / self.largeEffect
	end,

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		self.quad = love.graphics.newQuad(0, 0, 16, 16, 16, 16)
		self.img:setWrap("repeat", "clamp")
		self.pos.y = self.pos.y - 8

		local sfx = cache.soundData "sfx/chargeUp.ogg"
		local source = love.audio.newSource(sfx)
		source:setPitch(1.1)
		source:play()
	end,

	draw = function(self, progress)
		if not self.drawing then return end

		self.quad:setViewport(0, 0, 140*progress, 16)
		love.graphics.draw(self.img, self.quad, self.pos.x, self.pos.y, 0, self.dir, 1)
	end,
}
