local anim8 = require "anim8"

return class.private "IceAura" (Move)
{
	combo = {"short_a", "short_a", "long_a"},
	direct = false,
	fancyName = "Ice Aura",
	img = "gfx/spell/aura_ice.png",

	effect = function(self, player, enemy)
		player.defenseMultiplier = player.defenseMultiplier / self.largeEffect
	end,

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		local grid = anim8.newGrid(64, 64, 320, 192, 0, 0, 0)
		self.anim = anim8.newAnimation(grid('1-5', 1, '1-5', 2, '1-2', 3), 0.07, "pauseAtEnd")
		self.prev = 0
		self.pos = caster:getCenter() - vector(32, 32)

		if self.dir == -1 then
			self.pos.x = self.pos.x + 64
		end

		local sfx = cache.soundData "sfx/ice2.ogg"
		love.audio.newSource(sfx):play()
	end,

	draw = function(self, progress)
		if not self.drawing then return end

		local dt = progress - self.prev
		self.prev = progress
		self.anim:update(dt)

		self.anim:draw(self.img, self.pos.x, self.pos.y, 0, self.dir, 1)
	end,
}
