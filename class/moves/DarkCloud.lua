local anim8 = require "anim8"

return class.private "DarkCloud" (Move)
{
	combos =
	{
		{"short_a", "short_b", "short_a"},
		{"short_b", "short_a", "short_b"},
	},

	dmg = 10,
	direct = false,
	fancyName = "Dark Cloud",
	img = "gfx/spell/darkCloud.png",

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		local grid = anim8.newGrid(64, 64, 256, 192, 0, 0, 0)
		self.anim = anim8.newAnimation(grid('1-4', 1, '1-4', 2, '1-4', 3), 0.03125, "pauseAtEnd")
		self.prev = 0.5

		self.pos = target:getCenter() - vector(32, 32)
	end,

	effect = function(self)
		local sfx = cache.soundData "sfx/thunder.ogg"
		love.audio.newSource(sfx):play()
	end,

	draw = function(self, progress)
		if not self.drawing then return end
		if progress < 0.5 then return end

		local dt = progress - self.prev
		self.prev = progress
		self.anim:update(dt)

		self.anim:draw(self.img, self.pos.x, self.pos.y, 0, 1, 1)
	end,
}
