local anim8 = require "anim8"

return class.private "Counter" (Move)
{
	combos =
	{
		{ "long_a", "short_a" },
		{ "long_b", "short_b" },
	},
	img = "gfx/spell/counter.png",
	drawing = false,
	direct = false,

	block = function(self, other)
		if other.direct and other.dmg and other.dmg > 0 then
			local sfx = cache.soundData "sfx/ice2.ogg"
			love.audio.newSource(sfx):play()

			self.dmg = 14
			self.drawing = true
			return true
		end
		return false
	end,

	effect = function(self, player, enemy)
	end,

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		local grid = anim8.newGrid(16, 16, 80, 32, 0, 0, 0)
		self.anim = anim8.newAnimation(grid('1-5', 1, '1-5', 2), 0.05, "pauseAtEnd")
		self.prev = 0

		self.pos = caster:getCenter() - vector(8*self.dir, 8)
	end,

	draw = function(self, progress)
		if not self.drawing then self.prev = progress return end

		local dt = progress - self.prev
		self.prev = progress
		self.anim:update(dt)

		self.anim:draw(self.img, self.pos.x, self.pos.y, 0, self.dir, 1)
	end,
}
