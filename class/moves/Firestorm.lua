return class.private "Firestorm" (Move)
{
	combo = { "long_b", "long_b", "short_b" },
	dmg = 3,
	direct = false,
	img = "gfx/spell/particle_fire.png",

	effect = function(self, player, enemy)
		player.attackMultiplier = player.attackMultiplier * self.extraLargeEffect
		player.defenseMultiplier = player.defenseMultiplier * self.largeEffect
	end,

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		self.cloudImg = cache.image "gfx/spell/cloud.png"
		self.cloudQuad = love.graphics.newQuad(64, 64, 64, 64, self.cloudImg:getDimensions())
		self.pos = target:getFront() - vector(40*self.dir, 20)

		self.particles = {}
		for x = 1, 4 do
			for y = x % 2 == 0 and 1 or 2, 5, 2 do
				table.insert(self.particles, vector(x*4*self.dir, y*4))
			end
		end

		local sfx = cache.soundData "sfx/fire.ogg"
		love.audio.newSource(sfx):play()
	end,

	draw = function(self, progress)
		if not self.drawing then return end

		local xoff = self.dir*progress*40
		local yoff = progress%0.10*80

		for i, v in ipairs(self.particles) do
			love.graphics.draw(self.img, self.pos.x + v.x + xoff, self.pos.y + v.y + yoff, 0, 0.5)
		end
		love.graphics.draw(self.cloudImg, self.cloudQuad, self.pos.x-20*self.dir+xoff, self.pos.y-33, 0, self.dir, 1)
	end,
}
