local anim8 = require "anim8"

return class.private "Cloud" (Move)
{
	combo = {"short_a", "short_b"},
	dmg = 8,
	direct = false,
	img = "gfx/spell/cloud.png",

	effect = function(self, player, enemy)
		enemy.defenseMultiplier = enemy.defenseMultiplier * self.smallEffect
	end,

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		local grid = anim8.newGrid(64, 64, 256, 192, 0, 0, 0)
		self.anim = anim8.newAnimation(grid('1-4', 1, '1-4', 2, '1-4', 3), 0.03125, "pauseAtEnd")
		self.prev = 0.5

		self.pos = target:getCenter() - vector(32, 32)

		local sfx = cache.soundData "sfx/thunder.ogg"
		love.audio.newSource(sfx):play()
	end,

	draw = function(self, progress)
		if not self.drawing then return end
		if progress < 0.5 then return end

		local dt = progress - self.prev
		self.prev = progress
		self.anim:update(dt)

		self.anim:draw(self.img, self.pos.x, self.pos.y, 0, 1, 1)
	end,
}
