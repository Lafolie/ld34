local anim8 = require "anim8"

return class.private "Bluefire" (Move)
{
	combo = {"short_b", "short_a"},
	dmg = 8,
	img = "gfx/spell/bluefireMissile.png",
	hasHitAnimation = true,

	effect = function(self, player, enemy)
		enemy.attackMultiplier = enemy.attackMultiplier / self.smallEffect
	end,

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		local grid = anim8.newGrid(32, 16, 64, 16, 0, 0, 0)
		self.anim = anim8.newAnimation(grid('1-2', 1), 0.1)
		self.prev = 0
		self.target = target

		local sfx = cache.soundData "sfx/fireShort.ogg"
		love.audio.newSource(sfx):play()
	end,

	loadHitAnim = function(self)
		local grid = anim8.newGrid(64, 64, 256, 128, 0, 0, 0)
		self.anim = anim8.newAnimation(grid('1-4', 1, '1-4', 2), 0.025)
		self.img = cache.image "gfx/spell/bluefireFlames.png"

		self.prev = 1
		self.pos = self.target:getCenter() - vector(32, 32)
	end,

	draw = function(self, progress)
		if not self.drawing then return end

		if progress > 1 and self.prev <= 1 then
			self:loadHitAnim()
		end

		local dt = progress-self.prev
		self.prev = progress
		self.anim:update(dt)

		if progress <= 1 then
			self:adjustPos(progress)
			self.anim:draw(self.img, self.pos.x, self.pos.y-8, 0, self.dir, 1)
		else
			self.anim:draw(self.img, self.pos:unpack())
		end
	end,
}
