local anim8 = require "anim8"

return class.private "Firebomb" (Move)
{
	combo = {"short_b", "short_b"},
	dmg = 0,
	img = "gfx/spell/fireball.png",

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		self.balls = love.math.random(0, 3)
		self.dmg = self.balls * 6

		local grid = anim8.newGrid(16, 16, 64, 16, 0, 0, 0)
		self.anim = anim8.newAnimation(grid("1-4", 1), 0.05)
		self.prev = 0

		local sfx = cache.soundData "sfx/fireShort.ogg"
		love.audio.newSource(sfx):play()
	end,

	draw = function(self, progress)
		if not self.drawing then return end

		local dt = progress-self.prev
		self.prev = progress
		self.anim:update(dt)
		self:adjustPos(progress)

		if self.balls >= 2 then
			self.anim:draw(self.img, self.pos.x-3*self.dir, self.pos.y-11, 0, 0.8*self.dir, 0.8)
		end
		if self.balls >= 3 then
			self.anim:draw(self.img, self.pos.x-3*self.dir, self.pos.y-5, 0, 0.8*self.dir, 0.8)
		end
		if self.balls >= 1 then
			self.anim:draw(self.img, self.pos.x, self.pos.y-8, 0, self.dir, 1)
		end
	end
}
