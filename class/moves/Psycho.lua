local anim8 = require "anim8"

return class.private "Psycho" (Move)
{
	combos =
	{
		{ "long_a", "long_b", "short_b" },
		{ "long_b", "long_a", "short_b" },
	},
	direct = false,
	dmg = 20,
	img = "gfx/spell/psycho.png",

	effect = function(self, player, enemy)
		player.attackMultiplier = player.attackMultiplier / self.largeEffect
	end,

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		local grid = anim8.newGrid(240, 160, 1200, 640, 0, 0, 0)
		self.anim = anim8.newAnimation(grid("1-5", 1, "1-5", 2, "1-5", 3, "1-5", 4), 0.05)
		self.prev = 0

		local sfx = cache.soundData "sfx/psycho.ogg"
		love.audio.newSource(sfx):play()
	end,

	draw = function(self, progress)
		if not self.drawing then return end

		local dt = progress-self.prev
		self.prev = progress
		self.anim:update(dt)

		local x = self.dir == 1 and 0 or 240
		self.anim:draw(self.img, x, 0, 0, self.dir, 1)
	end
}
