return class.private "IceMissile" (Move)
{
	combo = {"short_a"},
	direct = false,
	dmg = 4,
	fancyName = "Ice Missile",
	img = "gfx/spell/iceMissile.png",

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		local particleImg = cache.image "gfx/spell/snowFlake.png"
		self.particles = love.graphics.newParticleSystem(particleImg, 256)
		self.particles:setParticleLifetime(0.05, 0.7)
		self.particles:setEmissionRate(40)
		self.particles:setSizes(0.8, 0.05)
		self.particles:setSizeVariation(1)
		self.particles:setSpeed(35)
		self.particles:setDirection(0.5*math.pi)
		self.particles:setRotation(-math.pi, math.pi)
		self.particles:setAreaSpread("normal", 6, 3)
		self.particles:setSpin(-0.25*math.pi, 0.25*math.pi)
		self.particles:start()

		self.prev = 0
	end,

	effect = function(self)
		local sfx = cache.soundData "sfx/ice.ogg"
		love.audio.newSource(sfx):play()
	end,

	draw = function(self, progress)
		if not self.drawing then return end
		if progress < 0.75 then return end
		progress = (progress - 0.75)*4

		local dt = progress - self.prev
		self.prev = progress
		self.particles:setPosition(self.pos.x-3*self.dir, self.pos.y+4)
		self.particles:update(dt)

		self:adjustPos(progress)
		love.graphics.draw(self.img, self.pos.x, self.pos.y, 0, self.dir, 1)
		love.graphics.draw(self.particles)
	end,
}
