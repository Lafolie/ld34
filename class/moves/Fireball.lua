local anim8 = require "anim8"

return class.private "Fireball" (Move)
{
	combo = {"short_b"},
	dmg = 6,
	img = "gfx/spell/fireball.png",

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		local grid = anim8.newGrid(16, 16, 64, 16, 0, 0, 0)
		self.anim = anim8.newAnimation(grid("1-4", 1), 0.05)
		self.prev = 0

		local sfx = cache.soundData "sfx/fireShort.ogg"
		love.audio.newSource(sfx):play()
	end,

	draw = function(self, progress)
		if not self.drawing then return end

		local dt = progress-self.prev
		self.prev = progress
		self.anim:update(dt)
		self:adjustPos(progress)
		self.anim:draw(self.img, self.pos.x, self.pos.y-8, 0, self.dir, 1)
	end
}
