local anim8 = require "anim8"

return class.private "Gift" (Move)
{
	combo = { "long_a", "long_a" },
	dmg = -25,
	direct = false,
	img = "gfx/spell/gift.png",

	effect = function(self, player, enemy)
		player.defenseMultiplier = player.defenseMultiplier / self.extraLargeEffect
	end,

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		local casterPos = caster:getFront()
		local grid = anim8.newGrid(8, 8, 24, 8, 0, 0, 0)
		self.anim = anim8.newAnimation(grid("1-3", 1), 0.05)
		self.prev = 0

		self.particles = {}
		for i = 1, 10 do
			local angle = love.math.random()*math.pi/2-math.pi/4
			local length = love.math.random()*6
			local x = casterPos.x + math.cos(angle)*length
			local y = casterPos.y + math.sin(angle)*length
			table.insert(self.particles, vector(x, y))
		end

		local sfx = cache.soundData "sfx/chargeUp.ogg"
		love.audio.newSource(sfx):play()
	end,

	draw = function(self, progress)
		if not self.drawing then return end

		local dt = progress-self.prev
		self.prev = progress
		self.anim:update(dt)

		for i, v in ipairs(self.particles) do
			self.anim:draw(self.img, v.x + 120*progress*self.dir, v.y, 0, 0.5)
		end
	end,
}
