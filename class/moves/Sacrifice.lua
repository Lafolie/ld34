local anim8 = require "anim8"

return class.private "Sacrifice" (Move)
{
	combo = { "long_b", "long_b" },
	direct = false,
	img = "gfx/spell/sacrifice.png",

	effect = function(self, player, enemy)
		player:doDamage(25)
		player.attackMultiplier = player.attackMultiplier * self.extraLargeEffect
	end,

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		self.pos = caster:getCenter()
		local grid = anim8.newGrid(8, 8, 24, 8, 0, 0, 0)
		self.anim = anim8.newAnimation(grid("1-3", 1), 0.05)
		self.prev = 0

		self.particles = {}
		for i = 1, 12 do
			local angle = 2*math.pi/12*i
			table.insert(self.particles, vector(math.cos(angle), math.sin(angle)))
		end

		local sfx = cache.soundData "sfx/chargeUp.ogg"
		love.audio.newSource(sfx):play()
	end,

	draw = function(self, progress)
		if not self.drawing then return end

		local dt = progress-self.prev
		self.prev = progress
		self.anim:update(dt)

		local dist = (1-progress)*25

		for i, v in ipairs(self.particles) do
			self.anim:draw(self.img, self.pos.x+v.x*dist, self.pos.y+v.y*dist, 0, 0.5)
		end
	end,
}
