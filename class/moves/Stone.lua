local anim8 = require "anim8"

return class.private "Stone" (Move)
{
	combos =
	{
		{ "long_a" },
		{ "long_b" },
	},

	dmg = 6,
	img = "gfx/spell/stone.png",

	effect = function(self, player, enemy)
		player.defenseMultiplier = player.defenseMultiplier / self.smallEffect

		local sfx = cache.soundData "sfx/boom.ogg"
		love.audio.newSource(sfx):play()
	end,

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		local grid = anim8.newGrid(64, 64, 320, 128, 0, 0, 0)
		self.anim = anim8.newAnimation(grid('1-5', 1, '1-5', 2), 0.05, "pauseAtEnd")
		self.prev = 0.5

		self.pos = target:getCenter() - vector(32, 32)
	end,

	draw = function(self, progress)
		if not self.drawing then return end
		if progress < 0.5 then return end

		local dt = progress - self.prev
		self.prev = progress
		self.anim:update(dt)

		self.anim:draw(self.img, self.pos.x, self.pos.y, 0, 1, 1)
	end,
}
