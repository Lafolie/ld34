local anim8 = require "anim8"

return class.private "Lance" (Move)
{
	combos =
	{
		{ "long_a", "long_b", "short_a" },
		{ "long_b", "long_a", "short_a" },
	},
	dmg = 12,
	img = "gfx/spell/lance.png",

	effect = function(self, player, other)
		local heal = self:getMultipliedDmg(player, other)/2
		player:doDamage(-heal)
		player.defenseMultiplier = player.defenseMultiplier * self.smallEffect

		local sfx = cache.soundData "sfx/ice.ogg"
		love.audio.newSource(sfx):play()
	end,

	__init__ = function(self, pos, dir, caster, target)
		Move.__init__(self, pos, dir, caster, target)

		local grid = anim8.newGrid(32, 16, 96, 16, 0, 0, 0)
		self.anim = anim8.newAnimation(grid("1-3", 1), 0.05)
		self.prev = 0
	end,

	draw = function(self, progress)
		if not self.drawing then return end

		local dt = progress-self.prev
		self.prev = progress
		self.anim:update(dt)
		self:adjustPos(progress)
		self.anim:draw(self.img, self.pos.x, self.pos.y-8, 0, self.dir, 1)
	end
}
