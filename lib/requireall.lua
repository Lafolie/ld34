return function(prefix)
	if not prefix:sub(-1, -1) ~= "." then prefix = prefix .. "." end
	local predir = prefix:gsub("%.", "/")
	local items = love.filesystem.getDirectoryItems(predir)

	local ret = {}

	for i, v in ipairs(items) do
		local basename = v:match("^(.+)%.lua$")
		if basename then
			ret[basename] = require(prefix .. basename)
		end
	end

	return ret
end
