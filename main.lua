local requireall = require "requireall"
local repler = require "repler"

do -- GLOBALS
	class = require "slither"
	vector = require "vector"
	Gamestate = require "gamestate"
	cache = require "cache"

	requireall "class"
	states = requireall "state"
	moves = requireall "class.moves"

	highlightcolor = {91, 201, 193}
	white = {255, 255, 255}
	black = {0, 0, 0}
end

function love.load(args)
	repler.load()
	Gamestate.registerEvents()

	love.graphics.setDefaultFilter("nearest", "nearest")

	Gamestate.switch(states[args[2] or "menu"], unpack(args, 3))
end

function love.keypressed(key)
	if key == "escape" then
		if Gamestate.current() ~= states.menu then
			if states.fight.music then states.fight.music:stop() end
			Gamestate.switch(states.menu)
		else
			return love.event.quit()
		end
	end
end

function love.draw()
	love.graphics.scale(4, 4)
end

local print = love.graphics.print
local printf = love.graphics.printf

function love.graphics.print(text, x, y, ...)
	love.graphics.origin()
	print(text, 4*x, 4*y, ...)
	return love.graphics.scale(4, 4)
end

function love.graphics.printf(text, x, y, size, ...)
	love.graphics.origin()
	printf(text, 4*x, 4*y, size*4, ...)
	return love.graphics.scale(4, 4)
end
